﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib1
{
    public interface myPen
    {
        void doDraw(string txt);
        void doWrite(string txt);
    }
}
