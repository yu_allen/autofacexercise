﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace myInterface
{
    public interface IPen
    {
        string doDraw(string txt);
        string doWrite(string txt);
    }

}
