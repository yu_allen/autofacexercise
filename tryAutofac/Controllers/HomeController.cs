﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using myInterface;

namespace tryAutofac.Controllers
{
    public class HomeController : Controller
    {
        private IPen _myPenService;
        public HomeController(IPen myPenService)
        {
            _myPenService = myPenService;
        }
        public ActionResult Index()
        {
            string DrawResult = _myPenService.doDraw("Hello");
            string WriteResult = _myPenService.doWrite("Hello");
            ViewBag.DrawResult = DrawResult;
            ViewBag.WriteResult = WriteResult;
            return View();

        }
    }
}